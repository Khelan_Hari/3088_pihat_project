PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
QFN50P300X500X80-25N
$EndINDEX
$MODULE QFN50P300X500X80-25N
Po 0 0 0 15 00000000 00000000 ~~
Li QFN50P300X500X80-25N
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -2.49968 -2.25081 0.250977 0.250977 0 0.05 N V 21 "QFN50P300X500X80-25N"
T1 -2.33173 2.44588 0.250048 0.250048 0 0.05 N V 21 "VAL**"
DS -2.5 -1.5 -2.5 1.5 0.127 27
DS -2.5 1.5 2.5 1.5 0.127 27
DS 2.5 1.5 2.5 -1.5 0.127 27
DS 2.5 -1.5 -2.5 -1.5 0.127 27
DS -2.5 -1.1 -2.5 -1.5 0.127 21
DS -2.5 -1.5 -2.1 -1.5 0.127 21
DS 2.1 -1.5 2.5 -1.5 0.127 21
DS 2.5 -1.5 2.5 -1.1 0.127 21
DS 2.5 1.1 2.5 1.5 0.127 21
DS 2.5 1.5 2.1 1.5 0.127 21
DS -2.5 1.1 -2.5 1.5 0.127 21
DS -2.5 1.5 -2.1 1.5 0.127 21
DS -3 -2 -3 2 0.05 26
DS -3 2 3 2 0.05 26
DS 3 2 3 -2 0.05 26
DS 3 -2 -3 -2 0.05 26
DC -2.25 1.75 -2.1 1.75 0 21
DP 0 0 0 0 4 0 23
Dl -1.94509 -0.87
Dl 1.87 -0.87
Dl 1.87 0.904936
Dl -1.94509 0.904936
DP 0 0 0 0 4 0 19
Dl 0.114082 -0.61
Dl 1.61 -0.61
Dl 1.61 -0.114082
Dl 0.114082 -0.114082
DP 0 0 0 0 4 0 19
Dl -1.66317 -0.61
Dl -0.11 -0.61
Dl -0.11 -0.113634
Dl -1.66317 -0.113634
DP 0 0 0 0 4 0 19
Dl -1.67567 0.11
Dl -0.11 0.11
Dl -0.11 0.634875
Dl -1.67567 0.634875
DP 0 0 0 0 4 0 19
Dl 0.115011 0.11
Dl 1.61 0.11
Dl 1.61 0.637786
Dl 0.115011 0.637786
$PAD
Sh "1" R 0.84 0.27 0 0 900
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.75 1.45
$EndPAD
$PAD
Sh "2" R 0.84 0.27 0 0 900
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.25 1.45
$EndPAD
$PAD
Sh "3" R 0.84 0.27 0 0 900
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.75 1.45
$EndPAD
$PAD
Sh "4" R 0.84 0.27 0 0 900
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.25 1.45
$EndPAD
$PAD
Sh "5" R 0.84 0.27 0 0 900
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.25 1.45
$EndPAD
$PAD
Sh "6" R 0.84 0.27 0 0 900
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.75 1.45
$EndPAD
$PAD
Sh "7" R 0.84 0.27 0 0 900
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.25 1.45
$EndPAD
$PAD
Sh "8" R 0.84 0.27 0 0 900
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.75 1.45
$EndPAD
$PAD
Sh "9" R 0.84 0.27 0 0 1800
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.45 0.75
$EndPAD
$PAD
Sh "10" R 0.84 0.27 0 0 1800
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.45 0.25
$EndPAD
$PAD
Sh "11" R 0.84 0.27 0 0 1800
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.45 -0.25
$EndPAD
$PAD
Sh "12" R 0.84 0.27 0 0 1800
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.45 -0.75
$EndPAD
$PAD
Sh "13" R 0.84 0.27 0 0 2700
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.75 -1.45
$EndPAD
$PAD
Sh "14" R 0.84 0.27 0 0 2700
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.25 -1.45
$EndPAD
$PAD
Sh "15" R 0.84 0.27 0 0 2700
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.75 -1.45
$EndPAD
$PAD
Sh "16" R 0.84 0.27 0 0 2700
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.25 -1.45
$EndPAD
$PAD
Sh "17" R 0.84 0.27 0 0 2700
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.25 -1.45
$EndPAD
$PAD
Sh "18" R 0.84 0.27 0 0 2700
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.75 -1.45
$EndPAD
$PAD
Sh "19" R 0.84 0.27 0 0 2700
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.25 -1.45
$EndPAD
$PAD
Sh "20" R 0.84 0.27 0 0 2700
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.75 -1.45
$EndPAD
$PAD
Sh "21" R 0.84 0.27 0 0 0
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.45 -0.75
$EndPAD
$PAD
Sh "22" R 0.84 0.27 0 0 0
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.45 -0.25
$EndPAD
$PAD
Sh "23" R 0.84 0.27 0 0 0
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.45 0.25
$EndPAD
$PAD
Sh "24" R 0.84 0.27 0 0 0
At SMD N 00088000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.45 0.75
$EndPAD
$PAD
Sh "25" R 3.65 1.65 0 0 0
At SMD N 00008000
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE QFN50P300X500X80-25N
