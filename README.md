# 3088_PiHat_Project

A full breakdown of the design process of a UPS PiHat.
This PiHat project will operate as a UPS for a PiZero so that if the Pi looses power, the PiHat will supply the Pi with enough power to give the user the user enough time to shut it down safely. This will be done through indicator LEDs.

See appropriate folders for KiCAD schematics or PCB files.

[Design References](https://gitlab.com/Khelan_Hari/3088_pihat_project/-/tree/master/Design): Contains Requirements, Specifications and SPICE Simulations

[PCB files](https://gitlab.com/Khelan_Hari/3088_pihat_project/-/tree/master/PCB%20Source%20Files): Contains KiCad schematics, PCB, Bill of Materials and manufacturing processes

[GettingStarted.md](https://gitlab.com/Khelan_Hari/3088_pihat_project/-/blob/master/PCB%20Source%20Files/GettingStarted.md)

[The PCB's Bill Of Materials](https://gitlab.com/Khelan_Hari/3088_pihat_project/-/blob/master/PCB%20Source%20Files/Final_Circuit_BOM.csv)
