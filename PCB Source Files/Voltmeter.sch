EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "Raspberry Pi Zero (W) uHAT Template Board"
Date "2021-06-11"
Rev "1.0"
Comp "THLALE002 HRXKHE001 WTHJOS001"
Comment1 "Group 19"
Comment2 "This Schematic is licensed under MIT Open Source License."
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7400 7500 0    50   ~ 0
Voltmeter Circuit for UPS microPiHat
Text Notes 10600 7650 0    50   ~ 0
1.0
Text Notes 8150 7650 0    50   ~ 0
4/06/2021
$Comp
L pspice:CAP C2
U 1 1 60C18412
P 1300 4150
F 0 "C2" H 1478 4196 50  0000 L CNN
F 1 "100n" H 1478 4105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1300 4150 50  0001 C CNN
F 3 "~" H 1300 4150 50  0001 C CNN
	1    1300 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R2
U 1 1 60C18D7D
P 1750 4050
F 0 "R2" H 1818 4096 50  0000 L CNN
F 1 "10k" H 1818 4005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1790 4040 50  0001 C CNN
F 3 "~" H 1750 4050 50  0001 C CNN
	1    1750 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R8
U 1 1 60C1922A
P 2400 3700
F 0 "R8" V 2195 3700 50  0000 C CNN
F 1 "2k" V 2286 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2440 3690 50  0001 C CNN
F 3 "~" H 2400 3700 50  0001 C CNN
	1    2400 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R3
U 1 1 60C19CB7
P 2200 3950
F 0 "R3" H 2268 3996 50  0000 L CNN
F 1 "10k" H 2268 3905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2240 3940 50  0001 C CNN
F 3 "~" H 2200 3950 50  0001 C CNN
	1    2200 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R4
U 1 1 60C19F87
P 2200 4550
F 0 "R4" H 2268 4596 50  0000 L CNN
F 1 "10k" H 2268 4505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2240 4540 50  0001 C CNN
F 3 "~" H 2200 4550 50  0001 C CNN
	1    2200 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R5
U 1 1 60C1A270
P 2200 5050
F 0 "R5" H 2268 5096 50  0000 L CNN
F 1 "10k" H 2268 5005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2240 5040 50  0001 C CNN
F 3 "~" H 2200 5050 50  0001 C CNN
	1    2200 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R6
U 1 1 60C1A598
P 2200 5500
F 0 "R6" H 2268 5546 50  0000 L CNN
F 1 "10k" H 2268 5455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2240 5490 50  0001 C CNN
F 3 "~" H 2200 5500 50  0001 C CNN
	1    2200 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R7
U 1 1 60C1A820
P 2200 5950
F 0 "R7" H 2268 5996 50  0000 L CNN
F 1 "10k" H 2268 5905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2240 5940 50  0001 C CNN
F 3 "~" H 2200 5950 50  0001 C CNN
	1    2200 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R9
U 1 1 60C1ACFC
P 2600 6000
F 0 "R9" H 2668 6046 50  0000 L CNN
F 1 "10k" H 2668 5955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2640 5990 50  0001 C CNN
F 3 "~" H 2600 6000 50  0001 C CNN
	1    2600 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 3900 1300 3700
Wire Wire Line
	1300 3700 1750 3700
Wire Wire Line
	1750 3900 1750 3700
Connection ~ 1750 3700
Wire Wire Line
	1750 3700 2200 3700
$Comp
L Device:D_Zener D1
U 1 1 60C23223
P 1750 4550
F 0 "D1" V 1704 4629 50  0000 L CNN
F 1 "3.6V" V 1795 4629 50  0000 L CNN
F 2 "Diode_SMD:D_MELF_Handsoldering" H 1750 4550 50  0001 C CNN
F 3 "~" H 1750 4550 50  0001 C CNN
	1    1750 4550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 60C23AE2
P 1300 4550
F 0 "#PWR01" H 1300 4300 50  0001 C CNN
F 1 "GND" H 1305 4377 50  0000 C CNN
F 2 "" H 1300 4550 50  0001 C CNN
F 3 "" H 1300 4550 50  0001 C CNN
	1    1300 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 60C23F7E
P 1750 4900
F 0 "#PWR02" H 1750 4650 50  0001 C CNN
F 1 "GND" H 1755 4727 50  0000 C CNN
F 2 "" H 1750 4900 50  0001 C CNN
F 3 "" H 1750 4900 50  0001 C CNN
	1    1750 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 60C24234
P 2600 6300
F 0 "#PWR05" H 2600 6050 50  0001 C CNN
F 1 "GND" H 2605 6127 50  0000 C CNN
F 2 "" H 2600 6300 50  0001 C CNN
F 3 "" H 2600 6300 50  0001 C CNN
	1    2600 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 60C244F6
P 2200 6300
F 0 "#PWR04" H 2200 6050 50  0001 C CNN
F 1 "GND" H 2205 6127 50  0000 C CNN
F 2 "" H 2200 6300 50  0001 C CNN
F 3 "" H 2200 6300 50  0001 C CNN
	1    2200 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3700 2200 3800
Wire Wire Line
	2200 4900 2200 4800
Wire Wire Line
	2200 5200 2200 5250
Wire Wire Line
	2200 5800 2200 5700
Wire Wire Line
	2200 6300 2200 6100
Wire Wire Line
	1750 4700 1750 4900
Wire Wire Line
	1300 4550 1300 4400
Wire Wire Line
	2600 6300 2600 6150
Wire Wire Line
	2600 3700 2600 5850
Wire Wire Line
	2600 3700 2550 3700
Wire Wire Line
	2200 3700 2250 3700
Connection ~ 2200 3700
Wire Wire Line
	2200 4250 2800 4250
Wire Wire Line
	2200 4250 2200 4400
Wire Wire Line
	2200 4800 2800 4800
Connection ~ 2200 4800
Wire Wire Line
	2200 4800 2200 4700
Wire Wire Line
	2200 5250 2750 5250
Connection ~ 2200 5250
Wire Wire Line
	2200 5250 2200 5350
Wire Wire Line
	2200 5700 2750 5700
Connection ~ 2200 5700
Wire Wire Line
	2200 5700 2200 5650
Wire Wire Line
	1750 4200 1750 4250
Wire Wire Line
	2200 4250 2200 4100
Connection ~ 2200 4250
Wire Wire Line
	1750 4250 1950 4250
Wire Wire Line
	1950 4250 1950 4150
Wire Wire Line
	1950 4150 2800 4150
Connection ~ 1750 4250
Wire Wire Line
	1750 4250 1750 4400
Connection ~ 1500 1000
Wire Wire Line
	1500 1000 1400 1000
$Comp
L power:GND #PWR06
U 1 1 60BAB3C5
P 8550 950
F 0 "#PWR06" H 8550 700 50  0001 C CNN
F 1 "GND" H 8555 777 50  0000 C CNN
F 2 "" H 8550 950 50  0001 C CNN
F 3 "" H 8550 950 50  0001 C CNN
	1    8550 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 850  8550 950 
Wire Wire Line
	1500 1300 1500 2900
Wire Wire Line
	1500 2900 1600 2900
Connection ~ 1500 2900
Wire Wire Line
	1500 1000 1800 1000
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 60C0E97C
P 8350 850
F 0 "J1" H 8430 842 50  0000 L CNN
F 1 "Conn_01x02" H 8430 751 50  0000 L CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x02_P2.00mm_Vertical" H 8350 850 50  0001 C CNN
F 3 "~" H 8350 850 50  0001 C CNN
	1    8350 850 
	-1   0    0    1   
$EndComp
NoConn ~ 3050 2450
NoConn ~ 2050 2150
NoConn ~ 2050 1950
NoConn ~ 2050 1850
NoConn ~ 2050 1650
Connection ~ 3900 1950
Wire Wire Line
	3050 1950 3900 1950
Connection ~ 4800 1450
Wire Wire Line
	3900 2100 4150 2100
Connection ~ 3900 2100
Wire Wire Line
	4900 1450 4800 1450
Wire Wire Line
	4900 1950 4900 1450
Wire Wire Line
	3900 1950 4900 1950
Wire Wire Line
	3900 2100 3900 1950
Wire Wire Line
	4150 2450 4150 2400
Wire Wire Line
	3700 2450 4150 2450
Wire Wire Line
	3700 2100 3900 2100
$Comp
L Device:C C6
U 1 1 60B802FC
P 4150 2250
F 0 "C6" H 4265 2296 50  0000 L CNN
F 1 "10p" H 4265 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4188 2100 50  0001 C CNN
F 3 "~" H 4150 2250 50  0001 C CNN
	1    4150 2250
	1    0    0    -1  
$EndComp
Connection ~ 3200 2900
Wire Wire Line
	3700 2900 3700 2800
Wire Wire Line
	3200 2900 3700 2900
Wire Wire Line
	3700 2450 3700 2400
Connection ~ 3700 2450
Wire Wire Line
	3400 2450 3700 2450
Wire Wire Line
	3400 2150 3400 2450
Wire Wire Line
	3050 2150 3400 2150
Wire Wire Line
	3700 2500 3700 2450
Wire Wire Line
	4800 1450 4600 1450
Wire Wire Line
	4800 1750 4800 1450
Wire Wire Line
	3050 1750 4800 1750
Wire Wire Line
	4200 1450 4300 1450
Connection ~ 4200 1450
Wire Wire Line
	4200 1650 4200 1450
Wire Wire Line
	3050 1650 4200 1650
$Comp
L Device:R_US R11
U 1 1 60B7BE16
P 3700 2650
F 0 "R11" H 3768 2696 50  0000 L CNN
F 1 "100k" H 3768 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3740 2640 50  0001 C CNN
F 3 "~" H 3700 2650 50  0001 C CNN
	1    3700 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R10
U 1 1 60B7AF97
P 3700 2250
F 0 "R10" H 3768 2296 50  0000 L CNN
F 1 "324k" H 3768 2205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3740 2240 50  0001 C CNN
F 3 "~" H 3700 2250 50  0001 C CNN
	1    3700 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 2600 2050 2650
Wire Wire Line
	1900 2600 2050 2600
Wire Wire Line
	1900 2900 1950 2900
Connection ~ 1900 2900
$Comp
L Device:C C4
U 1 1 60B795B0
P 1900 2750
F 0 "C4" H 2015 2796 50  0000 L CNN
F 1 "1u" H 2015 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1938 2600 50  0001 C CNN
F 3 "~" H 1900 2750 50  0001 C CNN
	1    1900 2750
	1    0    0    -1  
$EndComp
Connection ~ 1800 1150
Wire Wire Line
	1800 1000 1800 1150
$Comp
L Device:C C3
U 1 1 60B76737
P 1500 1150
F 0 "C3" H 1615 1196 50  0000 L CNN
F 1 "4.7u" H 1615 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1538 1000 50  0001 C CNN
F 3 "~" H 1500 1150 50  0001 C CNN
	1    1500 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 1450 4200 1450
Wire Wire Line
	3350 1450 3700 1450
$Comp
L Device:L L1
U 1 1 60B73152
P 3850 1450
F 0 "L1" V 4040 1450 50  0000 C CNN
F 1 "4.7u" V 3949 1450 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" H 3850 1450 50  0001 C CNN
F 3 "~" H 3850 1450 50  0001 C CNN
	1    3850 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1800 1250 1800 1150
Connection ~ 1800 1250
Wire Wire Line
	2050 1250 1800 1250
Wire Wire Line
	1800 1150 2050 1150
Wire Wire Line
	1800 1450 1800 1250
Wire Wire Line
	2050 1450 1800 1450
$Comp
L Device:R_US R12
U 1 1 60B6E84C
P 4450 1450
F 0 "R12" V 4245 1450 50  0000 C CNN
F 1 "50m" V 4336 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4490 1440 50  0001 C CNN
F 3 "~" H 4450 1450 50  0001 C CNN
	1    4450 1450
	0    1    1    0   
$EndComp
Connection ~ 3350 1450
Wire Wire Line
	3050 1450 3350 1450
Connection ~ 3350 1300
Wire Wire Line
	3200 1300 3350 1300
Wire Wire Line
	3200 1250 3200 1300
Wire Wire Line
	3050 1250 3200 1250
Wire Wire Line
	3350 1350 3350 1300
Connection ~ 3350 1350
Wire Wire Line
	3050 1350 3350 1350
Wire Wire Line
	3350 1450 3350 1350
Wire Wire Line
	3350 900  3350 1000
Wire Wire Line
	3150 900  3350 900 
Wire Wire Line
	3150 1150 3150 900 
Wire Wire Line
	3050 1150 3150 1150
Connection ~ 3200 2650
Wire Wire Line
	3200 2550 3200 2650
Wire Wire Line
	3050 2550 3200 2550
Connection ~ 1950 2900
Wire Wire Line
	1950 2900 3200 2900
Wire Wire Line
	3200 2650 3200 2900
Wire Wire Line
	3050 2650 3200 2650
Wire Wire Line
	1950 2350 2050 2350
Wire Wire Line
	1950 2250 1950 2350
Wire Wire Line
	1200 2250 1950 2250
Wire Wire Line
	1200 2350 1200 2250
Wire Wire Line
	1850 2450 2050 2450
Wire Wire Line
	1850 2350 1850 2450
Wire Wire Line
	1600 2350 1850 2350
Connection ~ 1600 2900
Wire Wire Line
	1200 2900 1500 2900
Wire Wire Line
	1200 2650 1200 2900
Wire Wire Line
	1950 2900 1950 3100
Wire Wire Line
	1600 2900 1900 2900
Wire Wire Line
	1600 2650 1600 2900
$Comp
L power:GND #PWR03
U 1 1 60B6B69F
P 1950 3100
F 0 "#PWR03" H 1950 2850 50  0001 C CNN
F 1 "GND" H 1955 2927 50  0000 C CNN
F 2 "" H 1950 3100 50  0001 C CNN
F 3 "" H 1950 3100 50  0001 C CNN
	1    1950 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R1
U 1 1 60B6ADEC
P 1600 2500
F 0 "R1" H 1668 2546 50  0000 L CNN
F 1 "41.2k" H 1668 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1640 2490 50  0001 C CNN
F 3 "~" H 1600 2500 50  0001 C CNN
	1    1600 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 60B69F7C
P 1200 2500
F 0 "C1" H 1315 2546 50  0000 L CNN
F 1 "0.01u" H 1315 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1238 2350 50  0001 C CNN
F 3 "~" H 1200 2500 50  0001 C CNN
	1    1200 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 60B68F36
P 3350 1150
F 0 "C5" H 3465 1196 50  0000 L CNN
F 1 ".1u" H 3465 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3388 1000 50  0001 C CNN
F 3 "~" H 3350 1150 50  0001 C CNN
	1    3350 1150
	1    0    0    -1  
$EndComp
Wire Notes Line
	600  6600 600  3450
Wire Notes Line
	7750 1750 7750 600 
Text Notes 5250 1200 0    50   ~ 0
Charging circuit for the battery, to ensure \nthat battery maintained at full charge when \nconnected to mains supply. \nUses an LT8611 Charging IC for voltage \nsensing as well as current limiting.\n
Text Notes 2950 3850 0    50   ~ 0
Voltmeter circuit, to check the battery percentage of \nthe connected battery in 20% intervals. \nOutputs connect to their respective status LED
Wire Notes Line
	600  3450 5050 3450
Wire Notes Line
	5050 3450 5050 6600
Wire Notes Line
	600  6600 5050 6600
Wire Notes Line
	600  3300 600  650 
Wire Notes Line
	600  3300 7000 3300
Wire Notes Line
	7000 3300 7000 650 
Wire Notes Line
	600  650  7000 650 
Text Notes 9000 1050 0    50   ~ 0
Connector allows the battery\nto be connected to the charging circuit\nConnector not polarity protected, to connector \nmust ensure the battery correctly oriented\nmechanically.
Wire Notes Line
	10800 600  10800 1750
Wire Notes Line
	7750 1750 10800 1750
Wire Notes Line
	7750 600  10800 600 
Text GLabel 1400 1000 0    50   Input ~ 0
PowerCircuitry
Text GLabel 2800 4150 2    50   Input ~ 0
100
Text GLabel 2800 4250 2    50   Input ~ 0
80
Text GLabel 2800 4800 2    50   Input ~ 0
60
Text GLabel 2750 5250 2    50   Input ~ 0
40
Text GLabel 2750 5700 2    50   Input ~ 0
20
Text GLabel 8550 750  2    50   Input ~ 0
Vbattery
Text GLabel 4900 1450 2    50   Input ~ 0
Vbattery
Text GLabel 1300 3700 0    50   Input ~ 0
PowerCircuitry
$Comp
L KiCad_Circuit-cache:eec_LT8611EUDDPBF U?
U 1 1 60C3D488
P 1950 1150
F 0 "U?" H 2550 1415 50  0000 C CNN
F 1 "eec_LT8611EUDDPBF" H 2550 1324 50  0000 C CNN
F 2 "Linear_Technology-LT8611EUDDPBF-*" H 1950 1550 50  0001 L CNN
F 3 "http://cds.linear.com/docs/en/datasheet/8611f.pdf" H 1950 1650 50  0001 L CNN
F 4 "Manufacturer URL" H 1950 1750 50  0001 L CNN "Component Link 1 Description"
F 5 "http://www.linear.com/" H 1950 1850 50  0001 L CNN "Component Link 1 URL"
F 6 "Package Specification" H 1950 1950 50  0001 L CNN "Component Link 3 Description"
F 7 "http://cds.linear.com/docs/en/packaging/(UDD24)%20QFN%2005-08-1833%20Rev%20%C3%98.pdf" H 1950 2050 50  0001 L CNN "Component Link 3 URL"
F 8 "200kHz - 2.2MHz" H 1950 2150 50  0001 L CNN "Frequency Adjust Range"
F 9 "Surface Mount" H 1950 2250 50  0001 L CNN "Mounting Technology"
F 10 "1" H 1950 2350 50  0001 L CNN "Number of Outputs"
F 11 "24-Pin Plastic QFN, Body 5 x 3 mm, Pitch 0.5 mm" H 1950 2450 50  0001 L CNN "Package Description"
F 12 "Rev. Ã ̃, 05/2011" H 1950 2550 50  0001 L CNN "Package Version"
F 13 "IC" H 1950 2650 50  0001 L CNN "category"
F 14 "4058442" H 1950 2750 50  0001 L CNN "ciiva ids"
F 15 "e1e8219966231756" H 1950 2850 50  0001 L CNN "library id"
F 16 "Linear Technology" H 1950 2950 50  0001 L CNN "manufacturer"
F 17 "UDD-24" H 1950 3050 50  0001 L CNN "package"
F 18 "1377839663" H 1950 3150 50  0001 L CNN "release date"
F 19 "Yes" H 1950 3250 50  0001 L CNN "rohs"
F 20 "3A37C67E-3EB8-45E9-B618-7D5E6B49F8BD" H 1950 3350 50  0001 L CNN "vault revision"
F 21 "yes" H 1950 3450 50  0001 L CNN "imported"
	1    1950 1150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
