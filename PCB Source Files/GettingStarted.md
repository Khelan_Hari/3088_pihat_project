# Power Circuitry



# Status LEDs

There are 6 LEDs on the top side of the microPiHat PCB. These LEDs represent the current charge level of the connected battery. When the connected battery is fully charged, all the LEDs except the one labelled "Charging" will be illuminated. Should the piHat have a connection to the mains power suppply and not be fully charged, the LED labelled "Charging" will turn on. Once the battery reaches full charge, the "100%" LED will turn on and the "Charging" LED will turn off.

When there is no mains power supply to the circuit, the battery will begin to power the Raspberry Pi and begin to lose charge. As there is no mains power supply, the "Charging" LED will be off. Once the battery begins to discharge, a LED will turn off for every 20% of full charge left i.e. shoudld the battery be 68% charged, the "20%", "40%" and "60%" LEDs will be illuminated.

Once the battery has discharge to a level below 20% of its full charge, the "20%" LED will begin to flash. This indiactes that the Raspberry Pi should either be turned off or connected to mains power supply immediately, to prevent an unsafe power off and damage to the Pi.

# Voltmeter Circuit and Battery Charger

The user should connect the battery to supply header to the 2 Pin connector labelled "Battery Connector" found on the right hand side of the board. Care should be taken to insure that the ground of the battery is connected to the pin closest to the label "Battery Connector".
