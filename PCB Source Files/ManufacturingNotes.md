**Manufacturing Notes**		 

When you are ready to manufacture the PCB, follow the steps below to acquire the Gerber files which you can then send to your manufacturer. 

See [BOM](https://gitlab.com/Khelan_Hari/3088_pihat_project/-/blob/master/PCB%20Source%20Files/Final_Circuit_BOM.csv) for components are required.			 

1. In PCB view, File → Plot → Gerber files. 

2. In the layers tab select the layers you want.  

Click OK 						 

3. Then for the final file (NC Drill File)  

4. In PCB view, File → Fabrication Outputs → Drill file. 

Click OK (That will give you 2 files)  

5. Convert from KiCAD to the format of your desired manufacturer.	
